package fr.ulille.iutinfo.teletp;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SuiviAdapter extends RecyclerView.Adapter<SuiviAdapter.ViewHolder> {
    final SuiviViewModel model;

    public SuiviAdapter(SuiviViewModel model) {
        this.model = model;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final View itemView;

        public CheckBox getBox() {
            return ((CheckBox) itemView.findViewById(R.id.checkBox));
        }

        public void setQuestion(String question) {
            TextView tv = ((TextView) itemView.findViewById(R.id.question));
            tv.setText(""+question);
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.itemView.setOnClickListener(view -> {
                select(getAdapterPosition());
            });
        }
    }

    @NonNull
    @Override
    public SuiviAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.question_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SuiviAdapter.ViewHolder holder, int position) {
        CheckBox check = holder.getBox();
        check.setChecked(false);
        holder.setQuestion(model.getQuestions(position));
    }

    @Override
    public int getItemCount() {
        return model.getApplication().getResources().getStringArray(R.array.list_questions).length;
    }

    private void select(int position) {
        Log.d("PIZZA", "Item " + position + " selected.");
    }

    // TODO Q7
}
