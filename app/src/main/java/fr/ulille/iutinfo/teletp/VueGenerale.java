package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.util.zip.Inflater;

public class VueGenerale extends Fragment {

    String salle;
    String poste;
    //static final String DISTANCIEL;
    public SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
         View v = inflater.inflate(R.layout.vue_generale, container, false);

        Spinner spSalle = (Spinner) v.findViewById(R.id.spSalle);
        ArrayAdapter adSalle = ArrayAdapter.createFromResource(this.getContext(), R.array.list_salles, android.R.layout.simple_spinner_item);
        spSalle.setAdapter(adSalle);
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Spinner spPoste = (Spinner) v.findViewById(R.id.spPoste);
        ArrayAdapter adPoste = ArrayAdapter.createFromResource(this.getContext(), R.array.list_postes, android.R.layout.simple_spinner_item);
        spPoste.setAdapter(adPoste);
        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return v;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final String DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        this.poste = "";
        this.salle = DISTANCIEL;
        this.model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            EditText tvLogin = getActivity().findViewById(R.id.tvLogin);
            model.setUsername(tvLogin.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });
        this.update();

        // TODO Q5.b
        // TODO Q9
    }

    public void update(){
        Spinner spSalle = (Spinner)getActivity().findViewById(R.id.spSalle);
        Spinner spPoste = (Spinner)getActivity().findViewById(R.id.spPoste);

        if("Distanciel".equals(spSalle.getSelectedItem().toString())){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            this.model.setLocalisation("Distanciel");
        }
        else{
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            this.model.setLocalisation(spSalle.getSelectedItem().toString()+" : "+spPoste.getSelectedItem().toString());
        }
    }
    // TODO Q9
}